iptables-info: iptables-info.o
	cc -O2 -o iptables-info -lip4tc -lip6tc -lxtables iptables-info.o

iptables-info.o: iptables-info.c
	cc -O2 -o iptables-info.o -c iptables-info.c

clean:
	rm -f iptables-info iptables-info.o
