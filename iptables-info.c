#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <linux/limits.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <xtables.h>
#include <libiptc/libiptc.h>
#include <libiptc/libip6tc.h>

/*=============================
* typedefines
*/

#define _P_NAME               "iptables-info"
#define _P_VERSION            "0.0.0"

#define _M_DEFAULT            0x0
#define _M_LIST_TABLES        0x1
#define _M_LIST_CHAINS        0x2

#define PROC_NET_BASE         "/proc/net/"
#define C_PROGRAMMERS_DISEASE 4096

// let's type struct NEVER. AGAIN.
typedef struct ipt_entry       ipt_entry_t;
typedef struct ipt_ip          ipt_ip_t;
typedef struct ip6t_entry      ip6t_entry_t;
typedef struct ip6t_ip6        ip6t_ip6_t;
typedef struct xt_counters     xt_counters_t;
typedef struct xt_entry_match  xt_entry_match_t;
typedef struct xt_entry_target xt_entry_target_t;
typedef struct xtables_match   xtables_match_t;
typedef struct xtables_target  xtables_target_t;
typedef struct xtc_handle      xtc_handle_t;

struct _app {
  int    mode;
  int    family;
  char   separator;
  char * table;
  char * chain;
};
typedef struct _app app_t;


/*=============================
* prototypes
*/

// you know what this is
void print_usage(void);

// common
void init(char *, uint8_t);
void printf_const_char_star(const char *);
void each_table(void (*)(const char *));
void print_family(char *);
void print_table(const char *);
void print_sys_chain(const char *, const char *, const char *, unsigned long long, unsigned long long);
void print_usr_chain(const char *, const char *, unsigned);
void print_chain_rule(
  const char *, const char *, int,
  unsigned long long, unsigned long long,
  const char *, int,
  int, uint8_t, int, int,
  int, const char *, int, const char *,
  int, const char *, const char *,
  int, const char *, const char *
);

// ipv4
int  each_af4_table(void (*)(const char *));
void each_af4_chain(const char *, void (*)(xtc_handle_t *, const char *, const char *));
void print_af4_chain_head(xtc_handle_t *, const char *, const char *);
void dump_af4_chains(const char *);
void dump_af4_table(const char *);
void dump_af4_chain(xtc_handle_t *, const char *, const char *);
void dump_af4_chain_rule(xtc_handle_t *, const char *, const char *, int, const ipt_entry_t *);
void dump_af4_chain_rule_e_opts(const ipt_entry_t *);
void dump_af4_chain_rule_t_opts(xtc_handle_t *, const char *, const ipt_entry_t *);

// ipv6
int  each_af6_table(void (*)(const char *));
void each_af6_chain(const char *, void (*)(xtc_handle_t *, const char *, const char *));
void print_af6_chain_head(xtc_handle_t *, const char *, const char *);
void dump_af6_chains(const char *);
void dump_af6_table(const char *);
void dump_af6_chain(xtc_handle_t *, const char *, const char *);
void dump_af6_chain_rule(xtc_handle_t *, const char *, const char *, int, const ip6t_entry_t *);
void dump_af6_chain_rule_e_opts(const ip6t_entry_t *);
void dump_af6_chain_rule_t_opts(xtc_handle_t *, const char *, const ip6t_entry_t *);


/*=============================
* globals
*/

// getopts to the chopter
static struct option g_chopter[] = {
  {.name = "ipv4",        .has_arg = 0, .val = '4'},
  {.name = "ipv6",        .has_arg = 0, .val = '6'},
  {.name = "chain",       .has_arg = 1, .val = 'c'},
  {.name = "list-chains", .has_arg = 1, .val = 'C'},
  {.name = "separator",   .has_arg = 1, .val = 's'},
  {.name = "table",       .has_arg = 1, .val = 't'},
  {.name = "list-tables", .has_arg = 0, .val = 'T'},
  {.name = "help",        .has_arg = 0, .val = '?'},
  {NULL},
};

// xtables linkage
struct xtables_globals g_xtables_globals = {
  .option_offset   = 0,
  .program_name    = _P_NAME,
  .program_version = _P_VERSION,
  .orig_opts       = g_chopter,
  .compat_rev      = xtables_compatible_revision,
};

// we'll pretend we aren't using globals by stuffing all of them in one
app_t t_app = {
  .mode      = _M_DEFAULT,
  .family    = AF_INET,
  .separator = '\t',
  .table     = NULL,
};
/*
 * And I saw, and behold a white horse: and he that
 * sat on him had a bow; and a crown was given unto him:
 * and he went forth conquering, and to conquer.
 */
// arrows->enabled
app_t * g_app = &t_app;


/*=============================
* main
*/

int
main(int argc, char ** argv) {
  int opt_c;
  int status;

  status = 0;
  do {
    opt_c = getopt_long(argc, argv, "46c:Cs:t:T?", g_chopter, NULL);
    switch(opt_c) {
    case '4':
      // we could define our own... but why?
      g_app->family = AF_INET;
      break;
    case '6':
      // we6 could define our6 own... but why6?
      g_app->family = AF_INET6;
      break;
    case 'c':
      g_app->chain = optarg;
      break;
    case 'C':
      g_app->mode = _M_LIST_CHAINS;
      break;
    case 's':
      g_app->separator = optarg[0];
      break;
    case 't':
      g_app->table = optarg;
      break;
    case 'T':
      g_app->mode = _M_LIST_TABLES;
      break;
    case '?':
      status = 1;
      break;
    }
  } while(opt_c != -1);

  if(status != 0) {
    print_usage();
    exit(status);
  }

  switch(g_app->mode) {
  case _M_LIST_TABLES:
    each_table(print_table);
    break;

  case _M_LIST_CHAINS:
    switch(g_app->family) {
    case AF_INET:  status = each_af4_table(dump_af4_chains); break;
    case AF_INET6: status = each_af6_table(dump_af6_chains); break;
    default:
      status = printf("api fault: unknown address family: %u\n", g_app->family);
    }
    break;

  case _M_DEFAULT:
  default:
    switch(g_app->family) {
    case AF_INET:  status = each_af4_table(dump_af4_table); break;
    case AF_INET6: status = each_af6_table(dump_af6_table); break;
    default:
      status = printf("api fault: unknown address family: %u\n", g_app->family);
    }
  }

  return status;
}


/*=============================
* usage
*/

void
print_usage() {
  printf("usage: iptables-info [OPTIONS]\n");
  printf("options:\n");
  printf("normal mode:\n");
  printf("  -4, --ipv4            dump ipv4 tables\n");
  printf("  -6, --ipv6            dump ipv6 tables\n");
  printf("  -s, --separator=CHAR  specifies the separator character for columns\n");
  printf("  -t, --table=STRING    only dump the table specified\n");
  printf("  -c, --chain=STRING    only dump the chain specified\n");
  printf("\n");
  printf("By default, all tables are scanned unless -t is given.\n");
  printf("If used alone, -c will return chains in all tables.\n");
  printf("\n");
  printf("query mode:\n");
  printf("  -T, --list-tables     lists available tables\n");
  printf("  -C, --list-chains     lists available chains\n");
  printf("\n");
  printf("-T ignores address family and lists all available tables.\n");
  printf("-C can be used with -t to list chains in a specific table.\n");
}


/*=============================
* common
*/

void
init(char * family, uint8_t n_proto) {
  int rc;

  rc = xtables_init_all(&g_xtables_globals, n_proto);
  if(rc < 0) {
    errx(1, "fatal: %s: xtables_init_all() failed\n", family);
  }
  print_family(family);
  return;
}

void
printf_const_char_star(const char * percent_s_const_char_star) {
  printf("%s\n", percent_s_const_char_star);
  return;
}

void
each_table(void (* on_iteration)(const char *)) {
  char * file = g_app->family == AF_INET ? "ip_tables_names" : "ip6_tables_names";
  char   path[PATH_MAX + 1];
  char   buf[C_PROGRAMMERS_DISEASE];
  char * now;
  char * pow;
  char * end;
  int    len;
  int    new;
  int    fd;

  memset(path, '\0', PATH_MAX + 1);
  strncpy(path, PROC_NET_BASE, PATH_MAX);
  strncat(path, file, PATH_MAX - strlen(path));

  fd = open(path, O_RDONLY);
  if(fd < 0) {
    err(1, "%s: open", path);
  }

  len = 0;
  do {
    new = read(fd, buf + len, C_PROGRAMMERS_DISEASE - len);
    if(new < 0) {
      err(1, "%s: read", path);
    }

  //===============\\        /*---
    len = len + new;            //
    end = buf + len;            //
    pow = now = buf;         /*
   +===============+      / *
   |
   |   Through me you pass into the city of woe:
   |   Through me you pass into eternal pain:
   |   Through me among the people lost for aye.
   |   Justice the founder of my fabric moved:
   |   To rear me was the task of power divine,
   |   Supremest wisdom and primeval love.
   |   Before me things create were none, save things
   |   Eternal, and eternal I endure.
   |
   |   All hope abandon, ye who enter here.
   |
  \*/

    while(now < end) {
      if('\n' == *now) {
        *now = '\0';
        if((now - pow) > XT_TABLE_MAXNAMELEN) {
          errx(1, "%s: %s: table name too long", path, pow);
        }
        len -= now - pow + 1;
        on_iteration(pow);
        pow = now + 1;
      }

      now++;
    }

    if(pow < end) {
      len = end - pow;
      if(len >= C_PROGRAMMERS_DISEASE) {
        errx(1, "%s: line too long", path);
      }
      memcpy(buf, pow, len);
    }
  } while(new != 0);

  if(close(fd) < 0) {
    err(1, "%s: close", path);
  }
  return;
}

void
print_family(char * family) {
  putchar('f');
  printf("%c%s", g_app->separator, family);
  putchar('\n');
  return;
}

void
print_table(const char * table) {
  putchar('t');
  printf("%c%s", g_app->separator, table);
  putchar('\n');
  return;
}

void
print_sys_chain(const char * table, const char * chain, const char * policy, unsigned long long packets, unsigned long long bytes) {
  char percent_c = g_app->separator;
  putchar('c');
  printf("%c%s",   percent_c, table);
  printf("%c%s",   percent_c, chain);
  printf("%c%c",   percent_c, 'p');
  printf("%c%s",   percent_c, policy);
  printf("%c%llu", percent_c, packets);
  printf("%c%llu", percent_c, bytes);
  putchar('\n');
  return;
}

void
print_usr_chain(const char * table, const char * chain, unsigned references) {
  char percent_c = g_app->separator;
  putchar('c');
  printf("%c%s", percent_c, table);
  printf("%c%s", percent_c, chain);
  printf("%c%c", percent_c, 'r');
  printf("%c%u", percent_c, references);
  putchar('\n');
  return;
}

#define _F_FLAG(f) ((f ? '+' : '.'))
#define _I_FLAG(f) ((f ? '!' : '-'))
void
print_chain_rule(const char * table, const char * chain, int index,
                 unsigned long long packets, unsigned long long bytes,
                 const char * target, int f_f_goto,
                 int f_i_proto, uint8_t n_proto, int f_i_frag, int f_f_frag,
                 int f_i_in, const char * i_iface, int f_i_out, const char * o_iface,
                 int f_i_src, const char * src, const char * src_mask,
                 int f_i_dst, const char * dst, const char * dst_mask) {
  char         percent_c = g_app->separator;
  const char * proto     = NULL;

  for(int i = 0; xtables_chain_protos[i].name != NULL; i++) {
    if(xtables_chain_protos[i].num == n_proto) {
      proto = xtables_chain_protos[i].name;
      break;
    }
  }

  putchar('r');
  printf("%c%s",   percent_c, table);
  printf("%c%s",   percent_c, chain);
  printf("%c%i",   percent_c, index);
  printf("%c%llu", percent_c, packets);
  printf("%c%llu", percent_c, bytes);
  printf("%c%s",   percent_c, target);
  printf("%c%c",   percent_c, _F_FLAG(f_f_goto));
  printf("%c%c",   percent_c, _I_FLAG(f_i_proto));
  if(proto != NULL) {
    printf("%c%s", percent_c, proto);
  }
  else {
    printf("%c%hu", percent_c, n_proto);
  }
  printf("%c%c",   percent_c, _I_FLAG(f_i_frag));
  printf("%c%c",   percent_c, _F_FLAG(f_f_frag));
  printf("%c%c",   percent_c, _I_FLAG(f_i_in));
  printf("%c%s",   percent_c, i_iface);
  printf("%c%c",   percent_c, _I_FLAG(f_i_out));
  printf("%c%s",   percent_c, o_iface);
  printf("%c%c",   percent_c, _I_FLAG(f_i_src));
  printf("%c%s",   percent_c, src);
  printf("%c%s",   percent_c, src_mask);
  printf("%c%c",   percent_c, _I_FLAG(f_i_dst));
  printf("%c%s",   percent_c, dst);
  printf("%c%s",   percent_c, dst_mask);

  return;
}
#undef _I_FLAG
#undef _F_FLAG

int
dump_match(const xt_entry_match_t * match, const void * cake) {
  const char *            name = match->u.user.name;
  const u_int8_t          rev  = match->u.user.revision;
  const xtables_match_t * ext;
  
  ext = xtables_find_match(name, XTF_TRY_LOAD, NULL);
  if(NULL == ext) {
    warn("%s unable to find match\n", name);
  }
  else {
    if(ext->save != NULL) {
      if(match->u.user.revision == ext->revision) {
        printf(" -m %s", NULL == ext->alias ? name : ext->alias(match));
        ext->save(cake, match);
      }
      else {
        warn("%s: unsupported match revision\n", name);
      }
    }
    else {
      warn("%s: no save information\n", name);
    }
  }
  return 0;
}


/*=============================
* ipv4
*/

int
each_af4_table(void (* on_iteration)(const char *)) {
  init("ipv4", NFPROTO_IPV4);
  if(NULL == g_app->table) {
    each_table(on_iteration);
  }
  else {
    on_iteration(g_app->table);
  }
  return 0;
}

void
each_af4_chain(const char * table, void (* on_iteration)(xtc_handle_t *, const char *, const char *)) {
  xtc_handle_t * handle;
  const char   * chain;

  handle = iptc_init(table);
  if(NULL == handle) {
    errx(1, "iptc_init(): error: %s\n", iptc_strerror(errno));
  }

  print_table(table);
  chain = iptc_first_chain(handle);
  while(chain) {
    if(NULL == g_app->chain || 0 == strcmp(g_app->chain, chain)) {
      on_iteration(handle, table, chain);
    }
    chain = iptc_next_chain(handle);
  }
  iptc_free(handle);
  return;
}

void
print_af4_chain_head(xtc_handle_t * handle, const char * table, const char * chain) {
  const char *  policy;
  xt_counters_t counters;
  unsigned      references;

  policy = iptc_get_policy(chain, &counters, handle);
  if(policy != NULL) {
    print_sys_chain(table, chain, policy, counters.pcnt, counters.bcnt);
  }
  else {
    iptc_get_references(&references, chain, handle);
    print_usr_chain(table, chain, references);
  }
  return;
}

void
dump_af4_chains(const char * table){
  each_af4_chain(table, print_af4_chain_head);
  return;
}

void
dump_af4_table(const char * table) {
  each_af4_chain(table, dump_af4_chain);
  return;
}

void
dump_af4_chain(xtc_handle_t * handle, const char * table, const char * chain) {
  int                 index = 0;
  const ipt_entry_t * rule;

  print_af4_chain_head(handle, table, chain);
  rule = iptc_first_rule(chain, handle);
  while(rule != NULL) {
    dump_af4_chain_rule(handle, table, chain, ++index, rule);
    rule = iptc_next_rule(rule, handle);
  }
  return;
}

void
dump_af4_chain_rule(xtc_handle_t * handle, const char * table, const char * chain, int index, const ipt_entry_t * rule) {
  const char * target = iptc_get_target(rule, handle);
  uint8_t      oflags = rule->ip.flags;
  uint8_t      iflags = rule->ip.invflags;

  print_chain_rule(
    table,
    chain,
    index,
    rule->counters.pcnt,
    rule->counters.bcnt,
    target,
    (oflags & IPT_F_GOTO),
    (iflags & XT_INV_PROTO),
    rule->ip.proto,
    (iflags & IPT_INV_FRAG),
    (oflags & IPT_F_FRAG),
    (iflags & IPT_INV_VIA_IN),
    (rule->ip.iniface[0] == '\0' ? "any" : rule->ip.iniface),
    (iflags & IPT_INV_VIA_OUT),
    (rule->ip.outiface[0] == '\0' ? "any" : rule->ip.outiface),
    iflags & IPT_INV_SRCIP,
    xtables_ipaddr_to_numeric(&rule->ip.src),
    xtables_ipmask_to_numeric(&rule->ip.smsk),
    iflags & IPT_INV_DSTIP,
    xtables_ipaddr_to_numeric(&rule->ip.dst),
    xtables_ipmask_to_numeric(&rule->ip.dmsk)
  );

  putchar(g_app->separator);
  dump_af4_chain_rule_t_opts(handle, target, rule);

  putchar(g_app->separator);
  dump_af4_chain_rule_e_opts(rule);

  putchar('\n');
  return;
}

void
dump_af4_chain_rule_t_opts(xtc_handle_t * handle, const char * target, const ipt_entry_t * rule) {
  const xtables_target_t *  x_target;
  const xt_entry_target_t * e_target;

  x_target = iptc_is_chain(target, handle)
    ? xtables_find_target(XT_STANDARD_TARGET, XTF_LOAD_MUST_SUCCEED)
    : xtables_find_target(target, XTF_TRY_LOAD)
  ;

  if(x_target != NULL) {
    e_target = ipt_get_target((ipt_entry_t *) rule);
    if(x_target->save != NULL && x_target->revision == e_target->u.user.revision) {
      putchar(' ');
      x_target->save(&rule->ip, e_target);
    }
    else if(x_target->print != NULL) {
      warn("%s: unsupported target revision\n", target);
    }
  }
  return;
}

void
dump_af4_chain_rule_e_opts(const ipt_entry_t * rule) {
  IPT_MATCH_ITERATE(rule, dump_match, &rule->ip);
  return;
}


/*=============================
* ipv6
*/

int
each_af6_table(void (* on_iteration)(const char *)) {
  init("ipv6", NFPROTO_IPV6);
  if(NULL == g_app->table) {
    each_table(on_iteration);
  }
  else {
    on_iteration(g_app->table);
  }
  return 0;
}

void
each_af6_chain(const char * table, void (* on_iteration)(xtc_handle_t *, const char *, const char *)) {
  xtc_handle_t * handle;
  const char   * chain;

  handle = ip6tc_init(table);
  if(NULL == handle) {
    errx(1, "error: %s\n", ip6tc_strerror(errno));
  }

  print_table(table);
  chain = ip6tc_first_chain(handle);
  while(chain != NULL) {
    if(NULL == g_app->chain || 0 == strcmp(g_app->chain, chain)) {
      on_iteration(handle, table, chain);
    }
    chain = ip6tc_next_chain(handle);
  }
  ip6tc_free(handle);
  return;
}

void
print_af6_chain_head(xtc_handle_t * handle, const char * table, const char * chain) {
  const char *  policy;
  xt_counters_t counters;
  unsigned      references;

  policy = ip6tc_get_policy(chain, &counters, handle);
  if(policy != NULL) {
    print_sys_chain(table, chain, policy, counters.pcnt, counters.bcnt);
  }
  else {
    ip6tc_get_references(&references, chain, handle);
    print_usr_chain(table, chain, references);
  }
  return;
}

void
dump_af6_chains(const char * table) {
  each_af6_chain(table, print_af6_chain_head);
  return;
}

void
dump_af6_table(const char * table) {
  each_af6_chain(table, dump_af6_chain);
  return;
}

void
dump_af6_chain(xtc_handle_t * handle, const char * table, const char * chain) {
  int                  index = 0;
  const ip6t_entry_t * rule;

  print_af6_chain_head(handle, table, chain);
  rule = ip6tc_first_rule(chain, handle);
  while(rule != NULL) {
    dump_af6_chain_rule(handle, table, chain, ++index, rule);
    rule = ip6tc_next_rule(rule, handle);
  }
  return;
}

void
dump_af6_chain_rule(xtc_handle_t * handle, const char * table, const char * chain, int index, const ip6t_entry_t * rule) {
  const char * target = ip6tc_get_target(rule, handle);
  uint8_t      oflags = rule->ipv6.flags;
  uint8_t      iflags = rule->ipv6.invflags;

  print_chain_rule(
    table,
    chain,
    index,
    rule->counters.pcnt,
    rule->counters.bcnt,
    target,
    (oflags & IPT_F_GOTO),
    (iflags & XT_INV_PROTO),
    rule->ipv6.proto,
    (iflags & IP6T_INV_FRAG),
    0,
    (iflags & IP6T_INV_VIA_IN),
    (rule->ipv6.iniface[0] == '\0' ? "any" : rule->ipv6.iniface),
    (iflags & IP6T_INV_VIA_OUT),
    (rule->ipv6.outiface[0] == '\0' ? "any" : rule->ipv6.outiface),
    (iflags & IP6T_INV_SRCIP),
    xtables_ip6addr_to_numeric(&rule->ipv6.src),
    xtables_ip6mask_to_numeric(&rule->ipv6.smsk),
    (iflags & IP6T_INV_DSTIP),
    xtables_ip6addr_to_numeric(&rule->ipv6.dst),
    xtables_ip6mask_to_numeric(&rule->ipv6.dmsk)
  );

  putchar(g_app->separator);
  dump_af6_chain_rule_t_opts(handle, target, rule);

  putchar(g_app->separator);
  dump_af6_chain_rule_e_opts(rule);

  putchar('\n');
  return;
}

void
dump_af6_chain_rule_t_opts(xtc_handle_t * handle, const char * target, const ip6t_entry_t * rule) {
  const xtables_target_t *  x_target;
  const xt_entry_target_t * e_target;

  x_target = ip6tc_is_chain(target, handle)
    ? xtables_find_target(XT_STANDARD_TARGET, XTF_LOAD_MUST_SUCCEED)
    : xtables_find_target(target, XTF_TRY_LOAD)
  ;

  if(x_target != NULL) {
    e_target = ip6t_get_target((ip6t_entry_t *) rule);
    if(x_target->save != NULL && x_target->revision == e_target->u.user.revision) {
      x_target->save(&rule->ipv6, e_target);
    }
    else if(x_target->print != NULL) {
      warn("%s: unsupported target revision\n", target);
    }
  }
  return;
}

void
dump_af6_chain_rule_e_opts(const ip6t_entry_t * rule) {
  IP6T_MATCH_ITERATE(rule, dump_match, &rule->ipv6);
  return;
}
